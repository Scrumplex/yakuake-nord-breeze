Nord Breeze Yakuake
-------------------
Yakuake Theme based on Breeze and Nord Color Themes

Background colors are from Nord, where as accent / highlight colors are from Breeze

This work is based on [Breeze Perfect Dark](https://github.com/noahadvs/yakuake-breeze_perfect_dark) by @noahadvs.

# Installation

```bash
git clone https://gitlab.com/Scrumplex/yakuake-nord-breeze.git ~/.local/share/yakuake/skins/yakuake-nord-breeze
```

If you installed from GitHub, you can update the theme with this (only works if you don't modify anything):

```bash
cd ~/.local/share/yakuake/skins/yakuake-nord-breeze
git fetch origin && git pull
```

# License

This work is licensed under the terms of the GNU General Public License 3.0: [LICENSE](LICENSE)
